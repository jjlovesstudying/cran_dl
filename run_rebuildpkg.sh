#!/bin/bash

rm -r ./oialabcran/src/contrib/PACKAGE*
rm -r ./oialabcran/bin/windows/contrib/3.5/PACKAGE*

docker run -it --rm           \
	-v ${PWD}/:/PROJ/     \
	jjlovesstudying/cran_downloadpkg Rscript /PROJ/rebuild_pkg.R
