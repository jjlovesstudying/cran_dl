# PROJ_CRAN_DL

Purpose: To download R libraries online so that they can be copied over for offline private CRAN repository. This is for R version 3.5.3.

### To download libraries:
- Modify `download.R` by specifying the R libraries you want to download.
- Then run `run_download.sh` to start the downloading.
- Once done, the downloaded libraries can be found in `oialabcran`.


## To rebuild package in offline CRAN repository:
- Depending on whether your CRAN repository is stored in container or volume, you may need to mount your oialabcran folder.
- Modify `run_rebuildpkg.sh` accordingly. You may want to insert script for deleting PACKAGE* in `oialabcran` folder.
- Run `run_rebuildpkg.sh`. This will rebuild PACKAGE*.
- Verify that `PACKAGE*` is created/modified.
