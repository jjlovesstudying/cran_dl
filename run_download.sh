#!/bin/bash
echo " "
echo "---------------------------------------------------------------- "
echo " Project: PROJ_CRAN_DL                                           "
echo " Purpose: This script will make use of the container to download "
echo "          the R libraries using download.R. The downloaded       "
echo "          libraries will be stored in oialabcran folder.         "
echo "-----------------------------------------------------------------"

docker run -it --rm           \
	-v ${PWD}/:/PROJ/     \
	jjlovesstudying/cran_downloadpkg Rscript /PROJ/download.R

echo "Congratulations! Your downloaded libraries can be found in oialabcran folder"
